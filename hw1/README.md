# Homework 1

For each problem in this homework create a new file containing the solution code. File
name should be `problem_n.py` where `n` is the number of problem for each solution.
Additionally, create a directory named `problem_n/` for each problem and save the 
image results to the appropriate directory for each problem. All images should be 
in `.png` format.

Afterwards, all results should be added to the README file inside the directory for 
each of the problems.
Report for each problem should contain resulting images with comments, 
as well as your explanation of the problem solution. For more info about *Markdown* 
syntax check [this document](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

You can structure your code however you like, but the code should be able to generate
all resulting images on single run.

Check the example problem for inspiration.

*NOTE: All problems are allowed to use openCV functions only for displaying, reading
and saving images!* All other functionality has to be implementend using python 
and numpy. The only exception is colorspace conversion.

### Setup

- Images dir has been added to the root of the project
- Solutions for all problems in this homework should be run on all images  in the 
  images directory, unless otherwise noted
- You have been given a `problem_example.py` program which has the code for 
  loading all images, as well as some additional utility functions
  - The code solves the example problem written in Example problem section 
  - Try to understand what the code does
  - The code is extensively commented, so you should be able to understand it
- You can copy relevant parts of this code for each problem you are solving
- Write the code for each problem and save it as `problem_n.py`
- You are free to structure your code however you like, but try to keep the solution 
  either in the `__main__` part of the file, or create additional functions
- Write the report for each problem in its own directory, similar to the report 
  written for in `problem_example/README.md`

### Grade allocation

- Problem 1 - 5%
- Problem 2 - 10%
- Problem 3 - 10%
- Problem 4 - 10%
- Problem 5 - 25%
- Problem 6 - 15%
- Problem 7 - 25%

Both the program code and the reports will be graded. Program has to 
generate all the required images on the first run. Grade will be deducted
if the program requires multiple runs or code changes between the runs 
to generate required images.


## Example problem

### Subsample

Create a program which will subsample the images by factors `2, 4, 8`. 
Program should be able to work with both grayscale and color images.
Save resulting images in following format: `image_filename_subN` where `N` is the
subsampling factor. 


## Problem 1

### Stripes

Create a program which will load all images from the images directory and save the 
resulting images with filename of type: `image_filename_V` and `image_filename_H`, where:

  - `image_filename_V` will be image which has every other column set to `0`, while
  - `image_filename_H` will be image which has every other row set to `0`

## Problem 2

### Fat stripes

Create a program which will load all images from the images directory and create 
images with stripes similar to previous problem. Difference is that in this 
case stripes have to have width larger than 1pixel. Test your program with stripes 
of widths `[8, 16, 32]`. Save the resulting images with the filename of type: 
`image_filename_VN` and `image_filename_HN`, where `V` and `H` denotes vertical or 
horizontal stripes, while `N` denotes the stripe width.

Example with stripe width of 16pixels:

![](problem2_exampleimage1.png)

## Problem 3

### Checkerboard

Create a program which will load all images from the images directory and add a 
checkerboard pattern to the images. Instead of checkerboard having black and white 
squares, your checkerboard pattern should only have black squares. Instead of white
squares, leave the original pixel data of the image. 

Example with checkerboard width of 32 pixels:

![](problem3_exampleimage1.png)

## Problem 4

### Inverted Checkerboard

Create a program which will load all images from the images directory and add a 
checkerboard pattern to the images. Instead of checkerboard having black and white 
squares, in your checkerboard pattern white squares will contain original pixel data, 
while the black squares will contain inverted pixel data. 

Example with inverted checkerboard width of 32 pixels:

![](problem4_exampleimage1.png)

## Problem 5

### Floyd-Steinberg Dithering

To improve the visual quality when quantising images one can use
_Floyd-Steinberg Dithering_, a special type of so-called _error diffusion_. It
distributes the quantisation error of a pixel to its unprocessed neighbouring
pixels as follows:

For each pixel $`(i,j)`$ from left to right and top to bottom:

1. quantise $` u_{i,j} `$ with a method implemented in Lab 4
1. compute the error $` e `$ between the original value and the quantised
value
1. distribute the error $` e `$ to the neighbouring pixels that are not yet
visited as follows:

$` u_{i+1,j} = u_{i+1,j} + \frac{7}{16} \times e `$

$` u_{i-1,j+1} = u_{i-1,j+1} + \frac{3}{16} \times e `$

$` u_{i,j+1} = u_{i,j+1} + \frac{5}{16} \times e `$

$` u_{i+1,j+1} = u_{i+1,j+1} + \frac{1}{16} \times e `$

- Perform the _Floyd-Steinberg Dithering_, according to the formula
above. Use all images from `images` directory and quantisation values 
of $`q \in [1, 2, 3] `$
- Save the images with the filename format ` image_filename_qfsd.png `,  where ` q ` is the
numeric value of $` q `$ used for that image.  
- Comment the visual quality of dithered images compared to
images in Problems 3 and 4  of  Lab 4, 
for all the values of $` q `$.


#### Hints: 

- In the formulas above $` i `$ is used to denote iteration on X axis
(columns), while $` j `$ is used to denote iteration on Y axis (rows), so
take care while implementing your algorithm.
- Common error in implementing the above algorithm is to first quantize the whole
image, and then to perform the error diffusion using the above formulas. Please
note that the error calculated in a single iteration is distributed to
neighbouring pixel values of the original image, not to the quantized pixel
values. Those neighbouring pixels, along with the corresponding error added,
get in turn quantized in their own iteration step, with their error diffused to
their neighbouring pixels, and so on...
- The above formulas are not trivial to implement using Numpy slicing notation.
You will probably have to use nested ` for ` loops. Please note that the performance 
will be very poor -  your program (probably) did not hang, it just takes a long time to do the 
computation.

## Problem 6 

### Subsampling with averaging

Subsampling is the process where the color information in the image is
sampled at a lower resolution than the original. When subsampling with a factor
of $` f `$, the resulting image has $` f^2 `$ times smaller pixel count than
the original image. Single pixel $` res(i,j) `$  in the resulting image is an
average of $`f \times f `$ corresponding pixels of the original image,
as expressed in the following formula: 

$` res\left( i,j \right) = \frac{1}{f^2} \times \sum_{p=i*f}^{i*f+f} \sum_{q=j*f}^{j*f+f} orig(p,q) `$


Dimensions of the original image should be a multiple of subsampling factor $` f `$. 
Crop the image to the appropriate dimensions. Example: let the subsampling factor be
$` f = 8 `$ and image size `WxH: 100x110`. Nearest multiple of $` 8 `$ to the image 
width is `96` and nearest multiple of $` 8 `$ to the image height is `104`. Thus,  
the dimensions of the cropped image should be `WxH: 96x104`.

#### Your task:

- Implement the ` subsample() ` function, which takes as input a 2D numpy image
array ( grayscale image or single channel of a color image ) and a scaling
factor $` f `$. 
- Your program should work on both grayscale and color images. For color images
run the ` subsample() ` function on each channel of the color image. You can 
easily test if the loaded image is color or grayscale with:  

```python
if len(image.shape) == 2:
    #this is grayscale image
elif len(image.shape) == 3:
    #this is color image
```

- The function should return a subsampled image of the same dimensions as the
original image. Even though subsampled image should be smaller than original
image, we can simulate subsampling by setting all corresponding pixels of the
resulting image to their average in the original image. The following output
should clarify the desired result:  

```
'Original image:'
[[ 0  1  2  3]
 [ 4  5  6  7]
 [ 8  9 10 11]
 [12 13 14 15]]

'Subsampled image:'
[[ 2  4]
 [10 12]]

'Simulated subsampling:'
[[ 2  2  4  4]
 [ 2  2  4  4]
 [10 10 12 12]
 [10 10 12 12]]
```

- Run you program on images `baboon` and `pepper` with subsampling factors
$` f \in [8, 16, 32] `$ and save the resulting images with the following format: 
` image_filename_subN `, where the `N` is the subsampling factor $` f `$ used for 
that image.


## Problem 7
### Color Spaces and Compression
 
A color model is an abstract mathematical model describing the way colors can
be represented as tuples of numbers, typically as three or four values or color
components. When this model is associated with a precise description of how the
components are to be interpreted (viewing conditions, etc.), the resulting set
of colors is called color space.
[[1]](https://en.wikipedia.org/wiki/Color_model)
[[2]](https://en.wikipedia.org/wiki/Color_space)

One such color space is YCbCr color space, used primarily in video and digital
photography systems. 
Y is the luminance component and Cb and Cr are the blue-difference and
red-difference chroma components. 
[[3]](https://en.wikipedia.org/wiki/YCbCr)

For more info on YCbCr color space see Lecture 4. and Wikipedia links
referenced above. 

Since human visual system is more sensitive to black-and-white
infromation then to color information, your task will be to simulate 
simple compression techinque by subsampling chroma channels and
converting the image back to BGR.

Note: Similar compression techinque is actually used widely today, mostly in
transmission of video material, and also as one of compression techniques in
JPEG encoding
 [[4]](https://en.wikipedia.org/wiki/Chroma_subsampling) .

#### Your task:

- Convert the images ` baboon ` and ` pepper ` to YCbCr color space using the following
code snippet:  

```python
  ycrcb = cv2.cvtColor(img, cv2.COLOR_BGR2YCR_CB)
```
- Subsample *chroma* channels of your YCbCr image by factor $` f \in [2, 4, 8, 16] `$.

- Convert the image back to BGR colorspace using the following snippet:

```
  bgr = cv2.cvtColor(ycrcb, cv2.COLOR_YCR_CB2BGR)
```

- Save the images as
`.png` files with the filename format ` image_filename_f_subs.png `,  where ` f ` is the
numeric value of $` f `$ subsampling factor used for that image.  

- Compare the obtained results visually, and compare them to the original
image. What can you observe?

- In your subjective experience of the _compressed_ images, what would be
maximum subsampling factor that would still give adequate result. How many bits
per pixel would such a _compression_ require? Please recall that in this
context RGB images need 24 bits per pixel (3x8 bits).


