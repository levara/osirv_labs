# Configuring VM for Tensorflow

To install necessary libraries for TensorFlow run:

```
./osirv-install.sh
```

# Running the notebook

```
jupyter notebook
```
